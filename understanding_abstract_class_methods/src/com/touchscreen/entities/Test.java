package com.touchscreen.entities;

public class Test {

	public static void main(String[] args) {
		
		//Here ---> classes are ---> HpNoteBook && DellNoteBook
		
		HpNoteBook HpNoteBookObject = new HpNoteBook();
		DellNoteBook DellNoteBookObject = new DellNoteBook();
		//Click and Scroll Method : 
		System.out.println("-------- HPNOTEBOOK OBJECT -----------");
		HpNoteBookObject.click();
		HpNoteBookObject.scroll();
		System.out.println("-------- DELLNOTEBOOK OBJECT ---------");
		DellNoteBookObject.click();
		DellNoteBookObject.scroll();
		System.out.println("***************************************");
	}

}
