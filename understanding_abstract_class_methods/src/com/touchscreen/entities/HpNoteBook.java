package com.touchscreen.entities;

public class HpNoteBook extends HP {
	@Override
	public void click() {
		System.out.println("This is Click Method In HpNoteBook abstracted from HP class");
	}
	
	public void thisModelFunction() {
		System.out.println("This is own Model Function");
	}
}
