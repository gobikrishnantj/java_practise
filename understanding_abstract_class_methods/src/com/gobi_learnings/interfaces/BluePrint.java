package com.gobi_learnings.interfaces;

public interface BluePrint {
	
	//Learning by intializing variable in this interface : 
	//Note every variables in the interface is ---> public static final
	
	int weight = 50 ;
	
	//This is Interface -->BluePrint
	//Method To Implement :
	public void stringPrinter(String stringToPrint) ;
	public void stringConcater(String str1 , String str2) ;
	public void charFinder(String strForfind , char charToFind) ;

}
