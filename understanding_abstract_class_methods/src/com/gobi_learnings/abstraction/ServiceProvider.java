package com.gobi_learnings.abstraction;


//U are able to extend abstract class : 

public class ServiceProvider extends AbstractBluePrint {

	@Override // It is kind of over riding [For Remember]
	public void stringPrinter() {
		
	}
	
	
	//THIS CLASS IS ABLE TO USE THE NORMAL METHOD IN ABSTRACT CLASS LIKE NORMAL ONE : 
	
	//SECOND IDEA : 
	// iF U ARE NOT GIVE THE DEF FO THE ABSTRACT METHOD IN THIS CLASS MEANS
	// U WANT TO MAKE THIS CLASS AS ABSTRACT
	//WHY ----> 
	// BECOZ IF U ARE NOT GIVE THE DEF MEANS THAT IS ERROR RIGHT U ARE NOT ABLE T CREATE
	//AN OBJECT SO THAT AGAIN U WANT TO MAKE THE CLASS AS ABSTRACT
	
	
}
