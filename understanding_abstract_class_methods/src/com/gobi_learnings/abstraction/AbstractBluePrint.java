package com.gobi_learnings.abstraction;

public abstract class AbstractBluePrint {
	
	public AbstractBluePrint() {
		// TODO Auto-generated constructor stub
	}
	
	// Above class is abstract 
	//Going to give abstract method and normal method or this class : 
	//Note : We are not able to create a instance from abstract class spr 
	
	public void stringConcater(String str1 , String str2) {
		System.out.println("Concated String" + str1 + str2);
	}
	
	public abstract void stringPrinter(); //KeyWord abstract is neccessary
	
	

}
