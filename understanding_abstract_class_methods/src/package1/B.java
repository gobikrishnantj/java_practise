package package1;

public abstract class B extends A {
	
	//When u put any class has abstract definitely u are going to 
	//use it in another via extension right !!
	
	//So Here the def for aGreeting[abstract method of a is not needed]
	
	//Normal method : 
	public void normalFunction_B() {
		System.out.println("This is Normal Function in B");
	}
	
	
	//Giving Def for Abstract method of class A[superclass or parent class]
//	@Override
//	public void aGreeting() {
//		System.out.println("This is def for abstract method of class A");
//	}
	
	//This is abstract methiod created in this class to check in test class
	public abstract void sprGreeting() ;
}
