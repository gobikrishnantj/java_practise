package package1;

//making this class as abstract :
public abstract class A {
	
	//Normal method : 
	public void normalFunction_A() {
		System.out.println("This is Normal Function in A");
	}
	
	//Abstract method : [Note : Abstract method dont have method def]
	public abstract void aGreeting();

}
