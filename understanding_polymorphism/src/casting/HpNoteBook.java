package casting;

public class HpNoteBook extends HP {

	public void StringConcater(String str1 , String str2) {
		System.out.println("=========================");
		System.out.println("Method Name : String Concater");
		System.out.println("Class Name : HpNoteBook");
		System.out.println("This is Child Class of HP");
		System.out.println("=========================");
	}
	
	public void hpNoteBookOwnMethod() {
		System.out.println("===========================");
		System.out.println("Method Name : hpNoteBookPwnMethod");
		System.out.println("Class Name : HpNoteBook");
		System.out.println("Hint : This is Own Method for this class");
		System.out.println("============================");
	}
	
}
