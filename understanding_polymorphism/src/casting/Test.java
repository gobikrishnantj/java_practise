package casting;

public class Test {
	
	public static void main(String[] args) {
		//Parent Class : 
		HP hp_obj = new HP();
		//Child Class :
		HpNoteBook hp_note_book = new HpNoteBook();
		
		//hp_note_book.StringConcater("Gobi", "Krishnan");
		//hp_obj.StringConcater(null, null);
		
		//Casting Practise : 
		//For that Runtime PolyMorphism is important : 
		
		//Parent ref for child Object : 
		HP pObj = hp_note_book ;
		//Child Have its own method parent didnt have that specific method : 
		//trying call that : 
		//pObj.hpNoteBookOwnMethod(); // Error ---> 
		
		HpNoteBook hpNoteBookNewObject = (HpNoteBook) hp_obj ; //Parent Object to Child Object downCasting
		hpNoteBookNewObject.StringConcater(null, null);
		
		//Why changing is not happening : 
		// hp_obj ----> having StringConcater
		
		//Changing an parent to child : 
		
		
	}

}
