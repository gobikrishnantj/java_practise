package RunTime;

public class Test {
	
	public static void whatVandi(Parent obj) {
		System.out.println("=== IN TEST CLASS WHAT VANDI FUNCTION ====");
		obj.ride();
	}

	public static void main(String[] args) {
		
		
		Bike bike = new Bike() ;
		Car car = new Car();
		Test.whatVandi(bike);
		Test.whatVandi(car);

	}

}
