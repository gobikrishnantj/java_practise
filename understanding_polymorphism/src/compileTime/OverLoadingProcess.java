
package compileTime;

public class OverLoadingProcess {
	
	
	public void add(int a , int b) {
		System.out.println("a + b = " + (a+b));
	}
	
	public void add(int a , int b , int c) {
		System.out.println("a + b + c = " + (a + b + c) );
	}

	public static void main(String[] args) {
		
		OverLoadingProcess obj = new OverLoadingProcess() ;
		
		System.out.println("{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}");
		obj.add(10, 10); 
		obj.add(10, 10, 10); 
		System.out.println("{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}");
	}

}
