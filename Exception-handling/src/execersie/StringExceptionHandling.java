package execersie;

public class StringExceptionHandling {
	
	public static void main(String[] args) {
		String str = "Gobi" ; 
		try {
			int strToint = Integer.parseInt(str);
		}catch(NumberFormatException exception) {
			System.out.println(exception.getMessage());
		}
	}

}
