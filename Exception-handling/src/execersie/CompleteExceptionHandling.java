package execersie;

import java.util.Scanner;

public class CompleteExceptionHandling {
	
	//Going to now about 
	/*
	 * try
	 * catch
	 * finaly
	 * throws
	 * throw
	 * */
	
	//Handling Execption in main CLass: 
	public static void main(String[] args) throws Exception {


		int i = 0 , j = 0 , c = 0;
		Scanner scanner = new Scanner(System.in) ;
		System.out.println("Enter i : ");
		i = scanner.nextInt();
		System.out.println("Enter j : ");
		j = scanner.nextInt();
		
// THIS IS METHOD 1 : ---> HAVING TRY CATCH INSIDE THE MAIN METHOD :
//		try {
//			c = i / j ;
//			System.out.println("===== AFTER OPERATION RUN SUCCESSFULLY ===========");
//			System.out.println(c);
//			System.out.println("==================================================");
//		}catch(Exception e) {
//			System.out.println("========================");
//			System.out.println("0 cannot be a denominator !!\nReson :There is no infinity in Computer");
//			System.out.println("Execption : " + e.getMessage());
//			System.out.println("========================");
//		}
// -----------------------------------------------------------------------------------------

// METHOD : 2---> HAVING TRY CATCH INSIDE ANY FUNCTION :
		//CompleteExceptionHandling.twoNumberDivider(i, j);
		//Checking after this call : 
		//System.out.println("After the completion of above function call below code is running spr fine");
// METHOD : 3 ---> USING THE "THROWS KEYWORD" IN MAIN METHOD  :
		//EVERY MAIN METHOD HAVE THROW STATEMENT IMPLICITLY : 
		//GOING TO RUN THE CRITICAL STATEMENT WITHOUT TRY CATCH : 
		//c = i / j ; // This exception handled by explicuit exception handler of JVM !!
		//System.out.println("------------------\nAfter the process : \nc : " + c +"\n---------------------");
// METHOD : 4 ----> USING THROWS IN ANY STATIC FUNCTION :
		//try {
		//	CompleteExceptionHandling.twoNumberDivider2(i, j);
		//}catch(Exception e) {
		//	System.out.println("By ZERO EXCEPTION DONT GIVE 0 AS DENOMINATOR");
		//}
 // METHOD : 5 ---> USING THROW IN ACTION IN MAIN METHOD : 
		if((i+j) == 10) {
			throw new Exception("This is 10 Error");
		}
		
	}
	
	public static void twoNumberDivider(int num , int den) {
		// Having try catch here !!
		int c = 0 ;
		try {
			c = num / den ;
			System.out.println("===== AFTER OPERATION RUN SUCCESSFULLY ===========");
			System.out.println(c);
			System.out.println("==================================================");
		}catch(Exception e) {
			System.out.println("========================");
			System.out.println("0 cannot be a denominator !!\nReson :There is no infinity in Computer");
			System.out.println("Execption : " + e.getMessage());
			System.out.println("========================");
		}
		
		//Same Here like in method 1: 
		//Note ---> After this function the statements or line below this
		//function call wil run asusual spr fine right 
	}
	
	// EXABLE FOR METHOD : 4
	public static void twoNumberDivider2(int num , int den) throws Exception {
		System.out.println("Inside the towNumberDivider2 Method");
		int c = 0 ;
		c = num / den ;
		System.out.println("===== AFTER OPERATION RUN SUCCESSFULLY ===========");
		System.out.println(c);
		System.out.println("==================================================");
	}
	

}
