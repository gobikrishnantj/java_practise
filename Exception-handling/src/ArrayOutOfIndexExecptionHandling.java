// Trying to understand the execption handling :
public class ArrayOutOfIndexExecptionHandling {
	
	public static void main(String[] args) {
		int[] myArray = new int[5];
		
		for (int i = 0; i < myArray.length; i++) {
			myArray[i] = i + 10 ;
		}
		
		try {
			for (int i = 0; i <= myArray.length; i++) {
				System.out.println(myArray[i]);
			}
		}catch(ArrayIndexOutOfBoundsException exception) {
			System.out.println("U are Searching for a index which is out of range for the given array !");
		}
	}

}
