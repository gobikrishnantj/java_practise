package bag1;

public class Linear_Search_Base {

	public static void main(String[] args) {
		//Understanding Linear Search : 
		int[] myArray = {12 , 46 , 67 , 98 , 0 , 78} ;
		int culprit = 78 ; 
		int ans = LinearSearch(myArray , culprit) ;
		System.out.println("*********************") ;
		System.out.println(ans) ;
		System.out.println("*********************") ;
	}
	
	static int LinearSearch(int[] myArray , int culprit) {
		//case : 1
		if(myArray.length == 0) {
			return -1 ;
		}
		//case : 2
		if(myArray[0] == culprit) return 0 ;
		//Case : 3
		for(int i = 0 ; i < myArray.length ; i++) {
			if(myArray[i] == culprit) {
				return i ;
			}
		}
		
		//case : 4
		return -1 ;
	}

}
