package bag1;

public class Linear_Search_2d_Array {
	public static void main(String[] args) {
		int target = 900 ; 
		int[][] myArray = {
				{12 , 56 , 89} ,
				{45 , 90 , 78} ,
				{67 , 53 , 21} ,
		} ;
		
		printIndexOfTarget(myArray , target) ;
	}
	
	static void printIndexOfTarget(int[][] myArray , int culprit) {
		if(myArray.length == 0) {
			System.out.println("This is Empty Array") ;
			return ;
		}
		for(int i = 0 ; i < myArray.length ; i++) {
			if(myArray[i].length > 0) {
				for(int j = 0 ; j < myArray[i].length ; j++) {
					if(myArray[i][j] == culprit) {
						System.out.println("Finded ---> " + " i---> " + i + " j---->" + j) ;
					}
				}
			}
		}
		
		System.out.println("No Items Found !!") ;
		return ;
		
	}
}
