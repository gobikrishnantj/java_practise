package bag1;

public class Linear_Search_String {
	
	public static void main(String[] args) {
		String myStr = "Gobikrishnan" ; 
		char culprit = 'K' ; 
		
		System.out.println(SearchInString(myStr , culprit)) ;
	}
	
	static int SearchInString(String myStr , char culprit) {
		if(myStr.length() == 0) {
			return  -1 ;
		}
		for(int i = 0 ; i < myStr.length() ; i++) {
			if(myStr.charAt(i) == culprit) {
				return i ;
			}
		}
		return -1 ;
	}

}
