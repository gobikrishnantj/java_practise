package leetcode_linear_search;

public class FindEvenDigits {

	public static void main(String[] args) {
		int[] myArray = {127 , 678 , -9084 , 89675} ;
		
		//Find even digit elem in this array ----> 
		System.out.println(evenDigitFinder(myArray)) ;
	}
	
	static int evenDigitFinder(int[] myArray) {
		if(myArray.length == 0) {
			return -1 ;
		}
		for(int i = 0 ; i < myArray.length ; i++) {
			String temp = Integer.toString(myArray[i]);
			int itemLen = myArray[i] >= 0 ? temp.length() : temp.length() - 1 ;
			if(itemLen % 2 == 0) {
				return i ;
			}
		}
		return -1 ;
	}

}
