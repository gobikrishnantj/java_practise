package com.gobi_learnings_inheritance.examble3;

public class Child extends Parent {
	
	@Override
	public void m1() {
		super.m1();
		System.out.println("In Child");
	}
	
	public void m2() {
		System.out.println("Check the visibility of this method in Parent class");
	}

}
