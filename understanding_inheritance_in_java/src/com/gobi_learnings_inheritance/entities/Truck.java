package com.gobi_learnings_inheritance.entities;

public class Truck extends Vechicle {
	
	public String truckModel = null;
	
	public Truck(String lc , String vt , String tm) {
		super(lc , vt);
		this.truckModel = tm;
	}
	
	public String getTruckModel() {
		return this.truckModel;
	}

}
