package com.gobi_learnings_inheritance.entities;

public class Car extends Vechicle {
	
	public String fuelType = null;
	
	public Car(String lc , String vt , String ft) {
		super(lc , vt);
		System.out.println("\n------- Car -------\n");
		System.out.println(this);
		System.out.println("--------------");
		this.fuelType = ft ;
	}
	
	public String getFuelType() {
		return this.fuelType;
	}

}
