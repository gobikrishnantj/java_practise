package com.gobi_learnings_inheritance.entities;

public class Test {
	
	public static void main(String[] args) {
		Car myCarObject = new Car("17eer020" , "FourWheeler" , "Petrol");
		Truck t = new Truck("17eer020" , "sixteenWheeler" , "Diesel");
		Vechicle v = myCarObject ;
		Vechicle vt = t ; //coz : truvk having the v's fields and methods
		Truck nt = (Truck) vt ;
		
		
		
		System.out.println("------------- OBJECT CREATION ------------");
		System.out.println(myCarObject.licenceNumber);
		System.out.println(myCarObject.vechicleType);
		System.out.println(myCarObject.fuelType);
		System.out.println("------------- OBJECT CREATION ------------");
		System.out.println("\n-------------  ------------\n");
		System.out.println("------------- REF OBJECT CREATION ------------");
		System.out.println(v.licenceNumber);
		System.out.println(v.vechicleType);
		//System.out.println(v.fuelType);
		System.out.println("------------- REF OBJECT CREATION ------------");
	}

}
