package encapsulation;

public class Test {

	public static void main(String[] args) {
		SuperParent obj = new SuperParent("Gobi", "Krishnan", "17EER020", 22);
		
		System.out.println("========== GET METHOD INVOCATION BEFOR SETTING NEW VALUES ======");
		allGetterCaller(obj);
		System.out.println("----------------------------------------------------------");
		
		System.out.println("============ SET METHOD INVOCATION ===========");
		allSettercaller(obj, "Jega", "Jeevan", "17EER024", 45);
		System.out.println("----------------------------------------------------------");
		
		System.out.println("========= AFTER ALL INVOCATION OF SETTER WITH NEW VALUES ========");
		allGetterCaller(obj);
		System.out.println("----------------------------------------");
		
	}
	
	public static void allGetterCaller(SuperParent obj) {
		System.out.println(obj.getFirstName());
		System.out.println(obj.getLastName());
		System.out.println(obj.getRollNumber());
		System.out.println(obj.getUserAge());
	}
	
	public static void allSettercaller(SuperParent obj , String fn , String ln , String rn , int a) {
		obj.setFirstName(fn);
		obj.setLastName(ln);
		obj.setRollNumber(rn);
		obj.setUserAge(a);
	}

}
