package encapsulation;

public class SuperParent {
	
	private String firstName = null ; 
	private String lastname= null ;
	private String rollNumber = null ;
	private int age = 0 ; 
	
	
	public SuperParent(String fn , String ln , String rn , int a) {
		this.firstName = fn ; 
		this.lastname = ln ; 
		this.rollNumber = rn ;
		this.age =a ;
	}
	
	public void setFirstName(String fn) {
		this.firstName = fn ;
	}
	
	public String getFirstName() {
		return this.firstName ;
	}
	
	public void setLastName(String ln) {
		this.lastname = ln ;
	}
	
	public String getLastName() {
		return this.lastname ;
	}
	
	public void setRollNumber(String rn) {
		this.rollNumber = rn ;
	}
	
	public String getRollNumber() {
		return this.rollNumber ;
	}
	
	public void setUserAge(int age) {
		this.age = age ;
	}
	
	public int getUserAge() {
		return this.age;
	}

}
