package practise;

public class A {
	
	//Creating a variable in four available Mode : 
	private int a = 100 ; 
	public int b = 200 ; 
	int c = 300 ;
	protected int d = 400 ;
	
	//After that we can create a main method : 
	//1 . To check its visibility in its own class : 
	
	public static void main(String[] args) {
		//Create a Object for this class here : 
		A a = new A() ;
		System.out.println("-------------------------------------");
		System.out.println(a.a);
		System.out.println(a.b);
		System.out.println(a.c);
		System.out.println(a.d);
		System.out.println("-------------------------------------");
		//See we are able to use every variable in this class ---> No problem here !!
	}

}
