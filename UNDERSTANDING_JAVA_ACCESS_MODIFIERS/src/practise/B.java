package practise;

public class B {
	
	public static void main(String[] args) {
		//Checking the visibility of class A values here in Class b --> Same Package : 
		A a = new A();
		System.out.println("-------------------------------------");
		 //    ---> System.out.println(a.a); // a is not available
		//Understand : So Private is only visible inside its own class 
		System.out.println(a.b);
		System.out.println(a.c);
		System.out.println(a.d);
		System.out.println("-------------------------------------");
	}

}
