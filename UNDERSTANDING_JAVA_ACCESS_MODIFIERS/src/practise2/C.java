package practise2;

//imports : 
import practise.A;

public class C {
	
	public static void main(String[] args) {
		A a = new A();
		System.out.println("-------------------------------------");
		//System.out.println(a.a); //Private not available
		System.out.println(a.b);
		//System.out.println(a.c); //See package level variable is only available in its package
		//System.out.println(a.d); //Protected is only available when the class A is extended here !!
		System.out.println("-------------------------------------");
	}

}
