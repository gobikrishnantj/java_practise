package abstractExamble;

public class C extends B {
	
	@Override
	public void secondMethod() {
		System.out.println("This is Second Method def given in secondMethod");
		this.firstMethod(); // implicitily add this.firstMethod
	}
	
	public void thirdMethod() {
		System.out.println("This is Third Normal Method");
	}
}
