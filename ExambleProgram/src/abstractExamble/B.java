package abstractExamble;

public abstract class B extends A {

	@Override
	public void firstMethod() {
		System.out.println("This is First method def given in Class B");
	}
	
	public abstract void secondMethod();
	
}
