package stringbag;

import java.util.Scanner;


public class RemovingSplCharInString {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		String inputString = scan.nextLine() ; 
		//System.out.println(RemovingSplCharInString.removeSpclChar(inputString)) ;
		System.out.println(RemovingSplCharInString.removeExtraSpace(inputString)) ;
	}
	
	public static String removeSpclChar(String str) {
		
		return str.replaceAll("[^a-zA-Z0-9]", ""); 
		//check each and every character if it is spcl char means then changing it to "" ;
		
	}
	
	public static String removeExtraSpace(String str) {
		return str.replaceAll("\\s+", " ") ; //Take the space 
		
	}

}
