package stringbag;

import java.util.Scanner;

//imports : 

public class CheckBinaryOrNotUsingRegExp {
	
	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		String inputNumber = null ;
		
		inputNumber = s.nextLine() ;
		
		if(inputNumber.matches("[01]+")) {
			System.out.println("Hey this is binary Number");
		}
		else if(inputNumber.matches("[0-9A-B]+")) {
			System.out.println("Hey this is Hexa Number");
		}
		else if(inputNumber.matches("[0-9]{2}[/.-]{1}[0-9]{2}[/.-]{1}[0-9]{4}")) {
			System.out.println("Hey this is a DOB");
		}
		else {
			System.out.println("Hey , Number is invalid");
		}
	}

}
