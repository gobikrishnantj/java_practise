package stringbag;

import java.util.Scanner;

public class EmailFinder {

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in); 
		String userEmailId = s.nextLine();
		//Meaning : 
		System.out.println("-----------------------------------");
		boolean isEmail = EmailFinder.checkIsGamil(userEmailId) ;
		if(isEmail) {
			System.out.println("Yes it is Valid Gmail Address");
			System.out.println("Username : " + EmailFinder.userNameFinder(userEmailId));
			System.out.println("Domain Name : " + EmailFinder.domainNameFinder(userEmailId));
		}
		else {
			System.out.println("Sry U entered is not an valid Gmail address");
		}
		System.out.println("-----------------------------------");
	}
	
	public static boolean checkIsGamil(String email) {
		//it is Important : 
		// ---> ^[a-z1-9+-_.]+@[a-z1-9+-_.]+$
		//Check for Gamil : 
		return email.matches("^[a-z1-9+-_.]+@[gmail]{5}+[a-z1-9+-_.]{4}$");
	}
	public static String userNameFinder(String email) {
		//To find the userName from email ; 
		
		String userName = email.substring(0 , email.indexOf("@"));
		return userName ;
	}
	public static String domainNameFinder(String email) {
		String domainName= email.substring(email.indexOf("@")+ 1 , email.indexOf("."));
		return domainName ;
	}

}
